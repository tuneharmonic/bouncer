export class Vector {
    public X: number;
    public Y: number;

    constructor(x: number = 0, y: number = 0) {
        this.X = x;
        this.Y = y;
    }

    add(vector: Vector){
        this.X += vector.X;
        this.Y += vector.Y;
    }

    isNotZero(): boolean {
        return this.X !== 0 && this.Y !== 0;
    }
}
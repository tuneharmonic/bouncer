import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { Vector } from '../vector';
import { Direction } from '../direction';

@Component({
  selector: 'app-ball',
  templateUrl: './ball.component.html',
  styleUrls: ['./ball.component.less']
})
export class BallComponent implements OnInit {
  @Input() gravity = 1;
  @Input() playAreaBounds: number[];
  @Input() fps: number;

  @Output() ballClicked = new EventEmitter<number>();
  bounces = 0;
  
  // Various compatible transform properties for different browsers
  transform = ["transform", "msTransform", "webkitTransform", "mozTransform", "oTransform"];
  
  bounceMagnitude = 30;
  colliders: number[];
  position = new Vector();
  velocity = new Vector();
  gravPosition = 0;

  private myElement: HTMLElement;
  private transformProp: string;
  private interval: NodeJS.Timer;

  constructor() { }

  ngOnInit() {
    // initialize
    this.setElement();
    this.setTransformProp();
    this.setColliders();

    this.interval = setInterval(() => {
      this.velocity.Y += this.gravity;    // calculate gravity-based change in velocity
      this.position.add(this.velocity);   // calculate position by velocity
      this.move();                        // apply movement
    }, 1000 / (this.fps || 20));
  }

  checkCollisions() {
    // left
    if (this.colliders[Direction.Left] + this.position.X < this.playAreaBounds[Direction.Left]) {
      this.velocity.X = 0;
      this.position.X = this.playAreaBounds[Direction.Left] - this.colliders[Direction.Left];
    }
    // top
    if (this.colliders[Direction.Top] + this.position.Y < this.playAreaBounds[Direction.Top]) {
      this.velocity.Y = 0;
      this.position.Y = this.playAreaBounds[Direction.Top] - this.colliders[Direction.Top];
    }
    // right
    if (this.colliders[Direction.Right] + this.position.X > this.playAreaBounds[Direction.Right]) {
      this.velocity.X = 0;
      this.position.X = this.playAreaBounds[Direction.Right] - this.colliders[Direction.Right];
    }
    // bottom
    if (this.colliders[Direction.Bottom] + this.position.Y > this.playAreaBounds[Direction.Bottom]) {
      this.resetBounces();
      this.velocity.Y = 0;
      this.position.Y = this.playAreaBounds[Direction.Bottom] - this.colliders[Direction.Bottom];
    }
  }

  move() {
    this.checkCollisions();

    // apply transform
    if (this.myElement && this.transformProp) {
      this.myElement.style[this.transformProp] = `translate3d(${this.position.X}px, ${this.position.Y}px, 0)`;
    } else {
      console.error("Can't determine element or transform...");
      if (this.interval) {
        clearInterval(this.interval);
      }
    }
  }

  setColliders() {
    if (this.myElement) {
      this.colliders = [
        this.myElement.clientLeft,                                // left
        this.myElement.clientTop,                                 // top
        this.myElement.clientLeft + this.myElement.clientWidth,   // right
        this.myElement.clientTop + this.myElement.clientHeight    // bottom
      ];
    }
  }

  setElement() {
    if (!this.myElement) {
      this.myElement = document.getElementById('ball');
    }
  }

  setTransformProp() {
    if (!this.transformProp) {
      for (var i = 0; i < this.transform.length; i++) {
        if (typeof document.body.style[this.transform[i]] != "undefined") {
            this.transformProp = this.transform[i];
        }
      }
    }
  }

  resetBounces() {
    this.bounces = 0;
    this.ballClicked.emit(this.bounces);
  }

  clickBall(e: MouseEvent) {
    this.ballClicked.emit(++this.bounces);

    // assumes a square / sphere
    let radius = (this.colliders[Direction.Right] - this.colliders[Direction.Left]) / 2;

    let xScalar = radius - e.offsetX;
    // let yScalar = radius - e.offsetY;

    let realX = (xScalar / radius) * this.bounceMagnitude;
    let realY = this.bounceMagnitude / -2; // (yScalar / radius) * this.bounceMagnitude;
    this.velocity = new Vector(realX, realY);
    console.log(this.velocity);
  }
}

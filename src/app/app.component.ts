import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  title = 'Bouncer';
  currentBounces = 0;
  maxBounces = 0;

  framesPerSecond = 30;
  gravity = 0.5;

  bounds: number[];

  ngOnInit() {
    this.setBounds();
  }

  setClicks(clicks: number) {
    if (clicks > this.maxBounces) {
      this.maxBounces = clicks;
    }
    this.currentBounces = clicks;
  }

  setBounds() {
    let playArea = document.getElementById('playArea');

    console.log('Width:', playArea.clientWidth);
    console.log('Height:', playArea.clientHeight);

    this.bounds = [
      0,                    // left
      0,                    // top
      playArea.clientWidth, // right
      playArea.clientHeight // bottom    
    ];
  }
}
